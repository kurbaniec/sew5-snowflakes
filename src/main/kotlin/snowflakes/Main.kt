package snowflakes

/**
 * Simple snowflakes-simulator.
 *
 * @author Kacper Urbaniec
 * @version 2019-09-18
 */

fun main() {
    val canvas = Canvas(200, 30, 100)
    canvas.run()
}