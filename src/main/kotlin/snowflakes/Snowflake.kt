package snowflakes

import java.util.concurrent.Semaphore
import java.util.concurrent.atomic.AtomicInteger

/**
 * Represents a simple snowflake.
 *
 * @author Kacper Urbaniec
 * @version 2019-09-18
 */
class Snowflake(
    var x: Int,
    private val bottom: Int,
    private val event: Semaphore,
    private val counter: AtomicInteger
): Runnable{
    var y = 0
    var end = false

    override fun run()  {
        event.acquire()

        while (!end) {
            when ((0..3).shuffled().first()) {
                0 -> x--
                1 -> {}
                2 -> x++
            }

            when ((0..2).shuffled().first()) {
                0 -> y++
                1 -> y+=2
            }

            if (y >= bottom) {
                y = bottom
                end = true
                counter.incrementAndGet()
            }

            Thread.sleep(300)
        }
    }
}