package snowflakes

import java.util.concurrent.Executors
import java.util.concurrent.Semaphore
import java.util.concurrent.atomic.AtomicInteger

/**
 * Represents an "canvas" that renders all snowflakes in the console.
 * Also initializes all snowflakes and checks their status.
 *
 * @author Kacper Urbaniec
 * @version 2019-09-18
 */
class Canvas(val width: Int, val height: Int, val count: Int) {
    private val executor = Executors.newCachedThreadPool()
    private val canvas = mutableListOf<MutableList<String>>()
    private val snowflakes = mutableListOf<Snowflake>()
    private val event = Semaphore(count)
    private val counter = AtomicInteger(0)
    private var done = false

    init {
        // Create "canvas" for terminal output
        for (i in 0 until height) {
            val empty = mutableListOf<String>()
            for (j in 0 until width) {
                empty.add(" ")
            }
            canvas.add(empty)
        }
        // Create event-lock
        event.acquire(count)
    }

    /**
     * Check if all snowflakes have reached the "ground".
     */
    private fun isEnd() {
        if (counter.get() == snowflakes.size) {
            done = true
            executor.shutdown()
        }
    }

    /**
     * Clear "canvas".
     */
    private fun clear() {
        val lastRow = canvas.size-1
        for(i in 0 until canvas.size) {
            for (j in 0 until canvas[i].size) {
                if (i == lastRow)
                    canvas[i][j] = "-"
                else
                    canvas[i][j] = " "
            }
        }
    }

    /**
     * Render snowflakes on "canvas".
     */
    private fun render() {
        for(flake in snowflakes) {
            val x = flake.x
            val y = flake.y
            if (x >= 0 && x <= width-1) {
                if (y >= 0 && y <= height-1) {
                    canvas[y][x] = "*"
                }
            }
        }
        for (row in canvas) {
            println(row.joinToString(separator = "") { it })
        }
    }


    /**
     * Initialize snowflakes and run render loop.
     */
    fun run() {
        for (i in 0 until count) {
            val flake = Snowflake((0 until width).shuffled().first(), height-1, event, counter)
            snowflakes.add(flake)
            executor.submit(flake)
        }

        println(""" 
 ______     __   __     ______     __     __     ______   __         ______     __  __     ______     ______
/\  ___\   /\ "-.\ \   /\  __ \   /\ \  _ \ \   /\  ___\ /\ \       /\  __ \   /\ \/ /    /\  ___\   /\  ___\
\ \___  \  \ \ \-.  \  \ \ \/\ \  \ \ \/ ".\ \  \ \  __\ \ \ \____  \ \  __ \  \ \  _"-.  \ \  __\   \ \___  \
 \/\_____\  \ \_\\"\_\  \ \_____\  \ \__/".~\_\  \ \_\    \ \_____\  \ \_\ \_\  \ \_\ \_\  \ \_____\  \/\_____\
  \/_____/   \/_/ \/_/   \/_____/   \/_/   \/_/   \/_/     \/_____/   \/_/\/_/   \/_/\/_/   \/_____/   \/_____/
        """)
        println("\n" + "-".repeat(height))

        // Release the snowflakes!
        Thread.sleep(1000)
        event.release(count)

        // Run render loop until program is done
        while (!done) {
            isEnd()
            clear()
            render()
            Thread.sleep(800)
        }

        println("""
 ______     __   __     _____    
/\  ___\   /\ "-.\ \   /\  __-.  
\ \  __\   \ \ \-.  \  \ \ \/\ \ 
 \ \_____\  \ \_\\"\_\  \ \____- 
  \/_____/   \/_/ \/_/   \/____/ 
            
        """)

    }

}