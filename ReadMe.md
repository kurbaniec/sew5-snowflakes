## Kacper Urbaniec | 5AHIT | 19.06.2019

# Snowflakes

## Aufgabenstellung

Sie wurden beauftragt, einen konsolenbasierten Bildschirmschoner zu implementieren, der Schneeflocken simuliert.

1. Implementieren Sie eine Klasse Schneeflocke, die einen Thread darstellt, und einen Parameter für die x-Position übernimmt

2. Nach dem Start eines Schneeflocken-Threads erhöht dieser sukzessive seine y-Position, wobei immer ein zufälliger Wechsel nach rechts und links möglich ist

3. Erreicht die Schneeflocke den Boden, wird ein globaler Counter erhöht, der über eine Lock geschützt wird

4. Der Main-Thread startet die Threads und visualisiert die aktuellen Positionen

5. Sobald alle Schneeflocken den Boden erreicht haben, wird das Programm beendet

6. (Erweiterung) Alle Schneeflocken warten vor dem Start auf ein Event, welches vom Main-Thread nach einer Sekunde ausgelöst wird.

## Program ausführen

```
gradle run
```

# Sources

* https://winterbe.com/posts/2015/04/30/java8-concurrency-tutorial-synchronized-locks-examples/ | 2019.09.25